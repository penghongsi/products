package cn.agilean.test.rest;

import cn.agilean.test.repo.BlackRepo;
import cn.agilean.test.service.CompanyManager;
import cn.agilean.test.vo.CompanyVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("companies")
public class CompanyApi {

    CompanyManager companyManager;
    BlackRepo repo;

    public CompanyApi(CompanyManager companyManager, BlackRepo repo) {
        this.companyManager = companyManager;
        this.repo = repo;
    }

    @GetMapping
    public List<CompanyVo> findAll() {
        return companyManager.findAll();
    }



}
