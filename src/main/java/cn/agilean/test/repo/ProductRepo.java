package cn.agilean.test.repo;

import cn.agilean.test.entity.Product;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Configuration
@ConfigurationProperties("products")
public class ProductRepo {
    private List<Product> products;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Optional<Product> getProduct(String productId) {
        if (productId == null) {
            throw new IllegalArgumentException("productId could not be null");
        }
        List<Product> products = getProducts();
        if (CollectionUtils.isEmpty(products)) {
            return Optional.empty();
        }
        return products.stream()
                .filter(p -> Objects.equals(p.getId(), productId))
                .findFirst();
    }
}
