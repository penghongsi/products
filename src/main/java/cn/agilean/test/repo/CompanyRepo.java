package cn.agilean.test.repo;

import cn.agilean.test.entity.Company;
import cn.agilean.test.entity.Relation;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Configuration
@ConfigurationProperties(prefix = "company")
public class CompanyRepo {
    private List<Company> companies;
    private List<Relation> relationShips;

    public List<Company> getCompanies() {
        if (companies == null) {
            return Collections.emptyList();
        }
        return companies;
    }

    public List<Relation> getRelationShips() {
        if (relationShips == null) {
            return Collections.emptyList();
        }
        return relationShips;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }

    public void setRelationShips(List<Relation> relationShips) {
        this.relationShips = relationShips;
    }

    public Optional<Company> getCompany(String companyId) {
        if (companyId == null) {
            throw new IllegalArgumentException("companyId could not be null");
        }
        List<Company> companies = getCompanies();
        if (CollectionUtils.isEmpty(companies)) {
            return Optional.empty();
        }
        return companies.stream()
                .filter(c -> Objects.equals(c.getId(), companyId))
                .findFirst();
    }
}
