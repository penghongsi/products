package cn.agilean.test.repo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.List;

@Configuration
@ConfigurationProperties("blacks")
public class BlackRepo {
    private List<String> companyBlackList;
    private List<String> productBlackList;

    public List<String> getCompanyBlackList() {
        if (companyBlackList == null) {
            return Collections.emptyList();
        }
        return companyBlackList;
    }

    public void setCompanyBlackList(List<String> companyBlackList) {
        this.companyBlackList = companyBlackList;
    }

    public List<String> getProductBlackList() {
        if (productBlackList == null) {
            return Collections.emptyList();
        }
        return productBlackList;
    }

    public void setProductBlackList(List<String> productBlackList) {
        this.productBlackList = productBlackList;
    }
}
