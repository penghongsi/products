package cn.agilean.test.stack;

import java.util.Arrays;

/**
 * 2022/3/15 10:19 PM
 *
 * @Author penghs
 **/
public class Stack {

    Object[] elements;
    private int count = 0;
    private int capacity = DEFAULT_CAPACITY;

    private final static int DEFAULT_CAPACITY = 16;//默认初始容量
    private final static float CAPACITY_FACTOR = 0.75f;//容量阈值，达到容量的0.75后，进行扩容
    private final static float GROW_FACTOR = 2.0f;//扩容2倍

    public Stack(int initialCapacity) {
        if (initialCapacity < 0)
            throw new IllegalArgumentException("initialCapacity is illegal:" + initialCapacity);
        this.capacity = initialCapacity;
        this.elements = new Object[capacity];
    }

    public Stack() {
        this.elements = new Object[capacity];
    }


    public Object push(Object e) {
        //1.确保有足够的容量
        ensureCapacity();
        elements[count++] = e;
        return e;
    }

    private void ensureCapacity() {
        //如果容量达到0.75，则以静默方式将数组扩容
        if (count >= elements.length * CAPACITY_FACTOR) {
            capacity = (int) (GROW_FACTOR * elements.length);
            elements = Arrays.copyOf(elements, capacity);
        }
    }

    public int getSize() {
        return this.count;
    }

    public int getCapacity() {
        return capacity;
    }

    public Object pop() {
        if (count <= 0) {
            throw new IllegalStateException("stack empty");
        }
        return elements[--count];
    }

}
