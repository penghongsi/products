package cn.agilean.test.service;

import cn.agilean.test.entity.Company;
import cn.agilean.test.entity.Relation;
import cn.agilean.test.repo.CompanyRepo;
import cn.agilean.test.vo.CompanyVo;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class CompanyManager {

    private final CompanyRepo companyRepo;

    public CompanyManager(CompanyRepo companyRepo) {
        this.companyRepo = companyRepo;
    }

    public List<CompanyVo> findAll() {
        List<Company> companies = companyRepo.getCompanies();
        if (CollectionUtils.isEmpty(companies)) {
            return Collections.emptyList();
        }
        Map<String, String> relationShips = companyRepo.getRelationShips()
                .stream()
                .collect(Collectors.toMap(Relation::getCompanyId, Relation::getCompanyId));
        return companies.stream()
                .filter(Objects::nonNull)
                .map(company -> convertVo(company, relationShips))
                .collect(Collectors.toList());
    }

    private CompanyVo convertVo(Company company, Map<String, String> relationShips) {
        CompanyVo companyVo = new CompanyVo();
        companyVo.setId(company.getId());
        companyVo.setName(company.getName());
        companyVo.setParent(getParent(company.getId(), relationShips));
        return companyVo;
    }

    @Nullable
    private CompanyVo getParent(String companyId, Map<String, String> relationShips) {
        String parentId = relationShips.get(companyId);
        if (parentId == null) {
            return null;
        }
        return companyRepo.getCompany(parentId)
                .map(company -> convertVo(company, Collections.emptyMap()))//不需要显示父级的父级，所以这里relationShips传空集
                .orElse(null);
    }
}
