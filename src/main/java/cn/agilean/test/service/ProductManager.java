package cn.agilean.test.service;

import cn.agilean.test.entity.Company;
import cn.agilean.test.entity.Product;
import cn.agilean.test.repo.BlackRepo;
import cn.agilean.test.repo.CompanyRepo;
import cn.agilean.test.repo.ProductRepo;
import cn.agilean.test.vo.CompanyVo;
import cn.agilean.test.vo.ProductVo;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Component
public class ProductManager {
    private final ProductRepo productRepo;
    private final CompanyRepo companyRepo;
    private final BlackRepo blackRepo;

    public ProductManager(ProductRepo productRepo, CompanyRepo companyRepo, BlackRepo blackRepo) {
        this.productRepo = productRepo;
        this.companyRepo = companyRepo;
        this.blackRepo = blackRepo;
    }

    public List<ProductVo> findAll() {
        List<ProductVo> productVos = productRepo.getProducts()
                .stream()
                .filter(Objects::nonNull)
                .map(this::convertVo)
                .collect(Collectors.toList());
        if (CollectionUtils.isEmpty(productVos)) {
            return productVos;
        }
        //过滤黑名单商品及黑名单公司的商品
        Set<String> blackCompanies = new HashSet<>(blackRepo.getCompanyBlackList());
        Set<String> blackProducts = new HashSet<>(blackRepo.getProductBlackList());
        Predicate<ProductVo> notInBlack = p -> {
            return !blackProducts.contains(p.getId()) && !blackCompanies.contains(p.getCompany().getId());
        };
        return productVos.stream()
                .filter(notInBlack)
                .collect(Collectors.toList());
    }

    public ProductVo getProduct(String id) {
        return productRepo.getProduct(id)
                .map(this::convertVo)
                .orElse(null);
    }

    private ProductVo convertVo(Product product) {
        ProductVo productVo = new ProductVo();
        productVo.setId(product.getId());
        productVo.setName(product.getName());
        productVo.setPrice(product.getPrice());
        CompanyVo companyVo = companyRepo.getCompany(product.getCompanyId())
                .map(this::convertVo)
                .orElse(null);
        productVo.setCompany(companyVo);
        return productVo;
    }

    private CompanyVo convertVo(Company company) {
        CompanyVo companyVo = new CompanyVo();
        companyVo.setId(company.getId());
        companyVo.setName(company.getName());
        return companyVo;
    }

}
