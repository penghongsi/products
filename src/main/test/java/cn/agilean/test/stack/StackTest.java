package cn.agilean.test.stack;

import org.junit.Assert;
import org.junit.Test;

/**
 * 2022/3/17 8:20 PM
 *
 * @Author penghs
 **/
public class StackTest {


    @Test
    public void testNew() {
        Stack stack = new Stack();
        Assert.assertTrue("预期elements不为空，且length==16", stack.elements != null && stack.elements.length == 16);
    }


    @Test
    public void testNewWithInitialCapacity() {
        Stack stack = new Stack(32);
        Assert.assertTrue("预期elements不为空，且length==32", stack.elements != null && stack.elements.length == 32);
    }

    @Test
    public void testPushAndPop() {
        Stack stack = new Stack();
        Assert.assertTrue("预期elements不为空，且length==16", stack.elements != null && stack.elements.length == 16);
        stack.push("a");
        stack.push("b");
        stack.push("c");
        Assert.assertEquals("预期size等于3", 3, stack.getSize());

        Object pop1 = stack.pop();
        Assert.assertEquals("pop1等于c", "c", pop1);
        Assert.assertEquals("预期size等于2", 2, stack.getSize());

        Object pop2 = stack.pop();
        Assert.assertEquals("预期size等于1", 1, stack.getSize());
        Assert.assertEquals("pop2等于b", "b", pop2);

        Object pop3 = stack.pop();
        Assert.assertEquals("预期size等于0", 0, stack.getSize());
        Assert.assertEquals("pop3等于a", "a", pop3);

        //继续pop则预期抛出异常
        Assert.assertThrows(IllegalStateException.class, stack::pop);
    }

    @Test
    public void testGrowCapacity() {
        Stack stack = new Stack();
        for (int i = 0; i <= 16 * 0.75; i++) {
            stack.push(i);
        }
        //预期扩容2倍==32
        Assert.assertEquals(32,stack.getCapacity());
    }


}
