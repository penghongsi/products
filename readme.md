### ready

1. checkout当前仓库
2. mock数据参考 [application.yml](src/main/resources/application.yml)
3. 代码: 按下列题目要求完成REST API
4. 时间: 1小时



### 题目1


#### 商品信息
| 字段        | 类型  | 备注 |
|-----------|-----|----|
| id        | 字符  | 商品唯一编码 |
| name      | 字符  | 商品名称 |
| price     | 数值  | 商品价格 |
| companyId | 字符  | 商家 |


#### 商家信息
| 字段        | 类型  | 备注     |
|-----------|-----|--------|
| id        | 字符  | 商家唯一编码 |
| name      | 字符  | 商家名称   |


#### 商家上下级关系
| 字段        | 类型  | 备注     |
|-----------|-----|--------|
| companyId | 字符  | 商家唯一编码 |
| parentId  | 字符  | 上级商家唯一编码 |



[ProductAPI](src/main/java/cn/agilean/test/rest/ProductApi.java)

[CompanyAPI](src/main/java/cn/agilean/test/rest/CompanyApi.java)

* test-1 增加按productId获取商品信息的restful api, 按如下要求输出

```
{
    "id":"xx",
    "name:"xxx",
    "price":xx,
    "company":{
        "id":"xx",
        "name":"xxx"
    }
}

```

* test-2 完成`/companies`, 按如下要求输出数据(没有父公司不输出"parent"字段)
[Relation](src/main/java/cn/agilean/test/entity/Relation.java)
```
[
    {
        "id": "c3",
        "name": "c3",
        "parent": 
        {
            "id": "c1",
            "name": "c1"
        }
    }
]
```

* test-3 增加`/products` restful api, 过滤掉被加入黑名单的商品，及黑名单中公司的商品(注意relationShip)
---


### 题目2 stack
 编码实现一个栈, 要求:
 1. push
 2. pop
 3. 适当考虑单元测试

 